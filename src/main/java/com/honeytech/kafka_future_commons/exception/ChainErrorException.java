package com.honeytech.kafka_future_commons.exception;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Exception with chain error data
 */
@AllArgsConstructor
@Getter
public class ChainErrorException extends Throwable {
    private ChainErrorContext context;
}
