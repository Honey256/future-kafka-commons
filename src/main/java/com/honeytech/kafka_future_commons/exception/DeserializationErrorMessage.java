package com.honeytech.kafka_future_commons.exception;

import com.honeytech.kafka_future_commons.model.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


/**
 * Message of deserialization error
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DeserializationErrorMessage implements Message {

    /**
     * Error message
     */
    private String message;

    /**
     * Exception
     */
    private Exception exception;
}
