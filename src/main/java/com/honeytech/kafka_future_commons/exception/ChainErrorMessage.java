package com.honeytech.kafka_future_commons.exception;


import com.honeytech.kafka_future_commons.model.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import static org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace;

/**
 * Message with chain error
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ChainErrorMessage implements Message {

    /**
     * Exception in chain
     */
    private String exception;

    /**
     * Message of exception
     */
    private String exceptionMessage;

    public ChainErrorMessage(Exception exception) {
        this.exception = getStackTrace(exception);
        this.exceptionMessage = exception.getMessage();
    }
}
