package com.honeytech.kafka_future_commons.exception;

/**
 * Async execution exception
 * throws when async execution failed
 */
public class AsyncExecutionException extends RuntimeException {
    public AsyncExecutionException() {
    }

    public AsyncExecutionException(String message) {
        super(message);
    }

    public AsyncExecutionException(String message, Throwable cause) {
        super(message, cause);
    }

    public AsyncExecutionException(Throwable cause) {
        super(cause);
    }

    public AsyncExecutionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
