package com.honeytech.kafka_future_commons.exception;

import com.honeytech.kafka_future_commons.model.ResponsePromise;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Map;
import java.util.UUID;

/**
 * Context of error in chain
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true, fluent = true)
public class ChainErrorContext {

    /**
     * Response promise
     */
    private ResponsePromise responsePromise;

    /**
     * Chain identifier
     */
    private UUID chainId;

    /**
     * Error
     */
    private ChainErrorMessage chainErrorMessage;

    /**
     * Error message headers
     */
    private Map<String, Object> headers;
}
