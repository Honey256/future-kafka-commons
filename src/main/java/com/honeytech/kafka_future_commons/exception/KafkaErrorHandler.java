package com.honeytech.kafka_future_commons.exception;

import com.honeytech.kafka_future_commons.model.Message;
import com.honeytech.kafka_future_commons.util.KafkaHeaderAccessor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ContainerAwareErrorHandler;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotNull;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Class intercepts all unchecked exceptions thrown while processing methods in consumer thread
 * annotated with {@link org.springframework.kafka.annotation.KafkaHandler}
 */
@Slf4j
@RequiredArgsConstructor
public class KafkaErrorHandler implements ContainerAwareErrorHandler {

    private final KafkaTemplate<String, Message> kafkaTemplate;

    private final String errorTopic;

    private final String sourceInstanceId;

    /**
     * Method intercepts all unchecked exceptions thrown while processing methods in consumer thread
     * annotated with {@link org.springframework.kafka.annotation.KafkaHandler}
     */
    @Override
    public void handle(Exception thrownException, List<ConsumerRecord<?, ?>> records, Consumer<?, ?> consumer, MessageListenerContainer container) {
        if (CollectionUtils.isEmpty(records)) {
            log.debug("FutureKafkaErrorHandler: no records found while processing error");
            return;
        }
        ConsumerRecord<?, ?> record = records.get(0);
        Map<TopicPartition, OffsetAndMetadata> commits = new LinkedHashMap<>();
        commits.put(new TopicPartition(record.topic(), record.partition()), new OffsetAndMetadata(record.offset() + 1));
        consumer.commitSync(commits);
        commits.forEach(((topicPartition, offsetAndMetadata) -> consumer.seek(topicPartition, offsetAndMetadata.offset())));

        if (record.value() instanceof DeserializationErrorMessage) {
            DeserializationErrorMessage error = (DeserializationErrorMessage) record.value();
            sendToErrorTopic(error.getMessage(), KafkaHeaderAccessor.ofHeaders(record.headers()), thrownException);
            sendToUpstreamService(KafkaHeaderAccessor.ofHeaders(record.headers()), thrownException);
            return;
        }
        log.error("Handling business exception", thrownException);
        sendToErrorTopic(record.value(), KafkaHeaderAccessor.ofHeaders(record.headers()), thrownException);
        sendToUpstreamService(KafkaHeaderAccessor.ofHeaders(record.headers()), thrownException);
    }

    /**
     * Method sends message to error topic
     * @param incomeMessage income message
     * @param incomeHeaders headers of income message
     * @param thrownException thrown exception
     */
    public <T> void sendToErrorTopic(T incomeMessage, KafkaHeaderAccessor incomeHeaders, Exception thrownException) {
        if (incomeHeaders.messageId() == null) {
            throw new IllegalArgumentException("Message id header is empty");
        }
        log.error("Sending message to error topic. Message id: {}", incomeHeaders.messageId());
        KafkaHeaderAccessor errorTopicHeaders = createErrorTopicHeaders(incomeHeaders, thrownException);
        this.kafkaTemplate.send(new ProducerRecord<>(
                errorTopic,
                null,
                incomeHeaders.messageId().toString(),
                (Message) incomeMessage,
                fillErrorTopicHeaders(errorTopicHeaders, thrownException).toKafkaHeaders()
        ));
    }

    /**
     * Method sends error to upstream service
     * @param incomeHeaders income request headers
     * @param thrownException error
     */
    public  void sendToUpstreamService(KafkaHeaderAccessor incomeHeaders, Exception thrownException) {
        if (incomeHeaders.messageId() == null) {
            throw new IllegalArgumentException("Message id header is empty");
        }
        log.error("Sending message to request source instance");


        KafkaHeaderAccessor errorHeaders = fillErrorTopicHeaders(KafkaHeaderAccessor.createIdentifiedWithSuccess(), thrownException)
                .chainId(incomeHeaders.chainId())
                .sourceInstanceId(this.sourceInstanceId)
                .errorServiceOccurred(this.sourceInstanceId);

        sendChainError(incomeHeaders, thrownException, errorHeaders);
    }

    /**
     * Method creates error topic headers
     * @param headers income headers
     * @param thrownException error
     * @return error topic headers
     */
    private KafkaHeaderAccessor createErrorTopicHeaders(KafkaHeaderAccessor headers, Exception thrownException) {
        return KafkaHeaderAccessor.create()
                .replyTopic(headers.replyTopic())
                .errorServiceOccurred(headers.errorServiceOccurred())
                .messageId(headers.messageId())
                .correlationId(headers.correlationId())
                .chainId(headers.chainId())
                .destinationInstanceId(headers.destinationInstanceId())
                .sourceInstanceId(headers.sourceInstanceId())
                .processingResult(-1)
                .errorDescription(thrownException.getMessage());
    }

    /**
     * Method fills error topic headers
     * @param errorTopicHeaders error topic headers
     * @param thrownException error
     * @return error topic headers
     */
    private KafkaHeaderAccessor fillErrorTopicHeaders(KafkaHeaderAccessor errorTopicHeaders, Exception thrownException) {
        //TODO must be implemented
        return errorTopicHeaders;
    }

    /**
     * Method sends error
     * @param headers request headers
     * @param exception error
     * @param errorHeaders error headers
     */
    private void sendChainError(@NotNull KafkaHeaderAccessor headers, @NotNull Exception exception, @NotNull KafkaHeaderAccessor errorHeaders) {
        if (MapUtils.isEmpty(headers.toMap())) {
            return;
        }
        if (StringUtils.isBlank(headers.replyTopic())) {
            throw new IllegalArgumentException("Reply topic is empty!");
        }
        log.error("Send error to {}", headers.sourceInstanceId());
        this.kafkaTemplate.send(new ProducerRecord<>(
                headers.replyTopic(),
                null,
                headers.messageId().toString(),
                new ChainErrorMessage(exception),
                errorHeaders.correlationId(headers.messageId())
                        .destinationInstanceId(headers.sourceInstanceId())
                        .toKafkaHeaders()));
    }
}
