package com.honeytech.kafka_future_commons.exception;

/**
 * Error of deserialization
 */
public class DeserializationException extends RuntimeException {
    public DeserializationException() {
        super();
    }

    public DeserializationException(String message) {
        super(message);
    }
}
