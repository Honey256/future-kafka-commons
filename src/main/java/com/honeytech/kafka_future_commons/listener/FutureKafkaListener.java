package com.honeytech.kafka_future_commons.listener;

import com.honeytech.kafka_future_commons.config.DefaultKafkaConfig;
import com.honeytech.kafka_future_commons.exception.*;
import com.honeytech.kafka_future_commons.model.Message;
import com.honeytech.kafka_future_commons.model.ResponsePromise;
import com.honeytech.kafka_future_commons.service.RequestResponseService;
import com.honeytech.kafka_future_commons.util.KafkaHeaderAccessor;
import com.honeytech.kafka_future_commons.util.KafkaHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * Base Kafka listener
 */
@Slf4j
@Validated
public abstract class FutureKafkaListener {

    @Autowired
    private RequestResponseService requestResponseService;

    @Autowired
    protected KafkaHelper kafkaHelper;

    @Autowired
    protected DefaultKafkaConfig kafkaConfig;

    @Autowired
    @Qualifier("messageKafkaTemplate")
    protected KafkaTemplate<String, Message> kafkaTemplate;


    /**
     * Base method for chain response processing
     * @param message response message
     * @param headers response headers
     * @param acknowledgment acknowledgement
     */
    protected void processResponse(Message message, @Headers Map<String, Object> headers, Acknowledgment acknowledgment) {
        KafkaHeaderAccessor responseHeaders = KafkaHeaderAccessor.ofMap(headers);
        UUID chainId = Optional.ofNullable(responseHeaders.chainId()).orElseThrow(() -> {
            log.error("Chain identifier is null, headers: {}", headers);
            return new IllegalStateException("Chain identifier is null");
        });
        String destinationInstanceId = responseHeaders.destinationInstanceId();
        if (!destinationInstanceId.equalsIgnoreCase(this.kafkaConfig.getSpecificConsumer().getGroupId())) {
            log.debug("chain id {}: Destination instance id: {}, this instance id: {}",
                    chainId,
                    destinationInstanceId,
                    kafkaConfig.getSpecificConsumer().getGroupId());
            acknowledgeRequest(acknowledgment);
            return;
        }
        logSpecificTopicRequest(message, headers);
        UUID messageId = responseHeaders.correlationId();
        log.debug("Chain id: {}; Message id: {}", chainId, messageId);
        this.requestResponseService.completeWithResponse(chainId, messageId, message, headers);
        acknowledgment.acknowledge();
    }

    /**
     * Method process deserialization error
     * @param deserializationErrorMessage fail message
     */
    @KafkaHandler
    public void errorHandler(DeserializationErrorMessage deserializationErrorMessage) {
        log.warn(deserializationErrorMessage.toString());//TODO check if can be used not only for deserialization error processing
        throw new DeserializationException(deserializationErrorMessage.toString());
    }

    /**
     * Method handles errors in requests chain
     * @param chainError error
     * @param headers error headers
     * @param acknowledgment acknowledgement
     */
    @KafkaHandler
    public void processChainError(@NotNull ChainErrorMessage chainError, @Headers Map<String, Object> headers, Acknowledgment acknowledgment) {
        try {
            KafkaHeaderAccessor chainErrorHeaders = KafkaHeaderAccessor.ofMap(headers);
            log.debug("Processing chain error with messageId {}, correlationId {}, chainId {}",
                    chainErrorHeaders.messageId(),
                    chainErrorHeaders.correlationId(),
                    chainErrorHeaders.chainId());

            ResponsePromise responsePromise = requestResponseService.getResponsePromise(chainErrorHeaders.chainId());

            if (responsePromise == null) {
                log.warn("Chain error receiver received, but response promise not found");
                return;
            }
            Optional.of(responsePromise)
                    .map(ResponsePromise::result)
                    .ifPresent(result -> {
                        result.completeExceptionally(new ChainErrorException(new ChainErrorContext()
                                .chainErrorMessage(chainError)
                                .chainId(chainErrorHeaders.chainId())
                                .headers(headers)
                                .responsePromise(responsePromise)));
                        requestResponseService.removeResponsePromise(chainErrorHeaders.chainId());
                    });
        } finally {
            acknowledgeRequest(acknowledgment);
        }
    }

    /**
     * Method acknowledges request
     */
    protected void acknowledgeRequest(Acknowledgment acknowledgment) {
        acknowledgment.acknowledge();
        log.trace("Request was acknowledged");
    }

    /**
     * Method logs request from specific topic
     * @param message message
     * @param headers headers
     */
    protected void logSpecificTopicRequest(Message message, Map<String, Object> headers) {
        log.info("[{}] Received request specific for instance {}",
                message.getClass().getSimpleName(),
                kafkaConfig.getSpecificConsumer().getGroupId());
        if (MapUtils.isNotEmpty(headers)) {
            kafkaHelper.printHeaders(headers);
        }
    }

    /**
     * Method logs request from specific topic
     * @param message message
     * @param headers headers
     */
    protected void logGroupTopicRequest(Message message, Map<String, Object> headers) {
        log.info("[{}] Received request group for instance {}",
                message.getClass().getSimpleName(),
                kafkaConfig.getSpecificConsumer().getGroupId());
        if (MapUtils.isNotEmpty(headers)) {
            kafkaHelper.printHeaders(headers);
        }
    }
}
