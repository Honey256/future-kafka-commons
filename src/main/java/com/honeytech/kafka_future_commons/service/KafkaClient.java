package com.honeytech.kafka_future_commons.service;

import com.honeytech.kafka_future_commons.model.AsyncResponse;
import com.honeytech.kafka_future_commons.model.Message;
import com.honeytech.kafka_future_commons.model.RequestParams;
import com.honeytech.kafka_future_commons.util.KafkaHeaderAccessor;
import com.honeytech.kafka_future_commons.util.KafkaHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * Exchange API for more comfortable kafka communication
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaClient {

    private final FutureKafkaTemplate<String, Message> futureKafkaTemplate;

    private final KafkaHelper kafkaHelper;

    /**
     * Method sends request and returns promise of Message
     * @param message message
     * @param topic topic
     * @param requestParams request parameters
     * @param <U> response type
     * @return response promise
     */
    public <U extends Message> CompletableFuture<U> exchange(@NotNull Message message,
                                                             @NotNull String topic,
                                                             @NotNull RequestParams requestParams) {
        return futureKafkaTemplate.send(kafkaHelper.prepareProducerRecord(message, topic), requestParams)
                .thenApply(KafkaHelper::getSingleResponseFromList);
    }

    /**
     * Method sends requests and returns promise of Messages
     * @param messages message
     * @param topic topic
     * @param requestParams request parameters
     * @param <U> response type
     * @return response promise
     */
    public <U extends Message> CompletableFuture<List<U>> exchange(@NotNull List<? extends Message> messages,
                                                                   @NotNull String topic,
                                                                   @NotNull RequestParams requestParams) {
        return futureKafkaTemplate.send(
                messages.stream()
                        .map(message -> kafkaHelper.prepareProducerRecord(message, topic))
                        .collect(Collectors.toList()),
                requestParams)
                .thenApply(KafkaHelper::getAllResponsesFromList);
    }


    /**
     * Method sends collection of messages and returns promise of Message
     * @param messages message
     * @param topic topic
     * @param requestParams request parameters
     * @param <U> response type
     * @return response promise
     */
    @SuppressWarnings("unchecked")
    public <U extends Message> CompletableFuture<List<AsyncResponse<U>>> exchange(@NotNull List<? extends Message> messages,
                                                                                  @NotNull String topic,
                                                                                  @NotNull RequestParams requestParams,
                                                                                  @NotNull Class<U> responseType) {
        return futureKafkaTemplate.send(
                        kafkaHelper.prepareProducerRecords(messages, topic, requestParams),
                        requestParams)
                .thenApply(asyncResponses -> asyncResponses.stream()
                        .map(asyncResponse -> (AsyncResponse<U>) asyncResponse)
                        .collect(Collectors.toList()));
    }

    /**
     * Method sends request and returns promise
     * @param message message
     * @param topic topic
     * @param requestParams request parameters
     * @param <U> response type
     * @return response promise
     */
    @SuppressWarnings("unchecked")
    public <U extends Message> CompletableFuture<AsyncResponse<U>> exchange(@NotNull Message message,
                                                                            @NotNull String topic,
                                                                            @NotNull RequestParams requestParams,
                                                                            @NotNull Class<U> responseType) {
        return futureKafkaTemplate.send(kafkaHelper.prepareProducerRecord(message, topic), requestParams)
                .thenApply(response ->
                        Optional.ofNullable(response)
                                .map(asyncResponses -> asyncResponses.get(0))
                                .map(asyncResponse -> (AsyncResponse<U>) asyncResponse)
                                .orElseThrow(() -> new IllegalArgumentException("Response message is null")));
    }


    /**
     * Method sends request message to kafka
     * @param message message
     * @param headers headers
     */
    public void send(Message message, Map<String, Object> headers) {
        KafkaHeaderAccessor kafkaHeaderAccessor = KafkaHeaderAccessor.ofMap(headers);
        ProducerRecord<String, Message> producerRecord = new ProducerRecord<>(
                kafkaHeaderAccessor.topic(),
                null,
                UUID.randomUUID().toString(),
                message,
                kafkaHeaderAccessor.toKafkaHeaders());
        log.info("Send message to {}", kafkaHeaderAccessor.topic());
        this.futureKafkaTemplate.getKafkaTemplate().send(producerRecord);
    }
}
