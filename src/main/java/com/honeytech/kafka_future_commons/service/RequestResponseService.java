package com.honeytech.kafka_future_commons.service;

import com.honeytech.kafka_future_commons.model.AsyncResponse;
import com.honeytech.kafka_future_commons.model.Message;
import com.honeytech.kafka_future_commons.model.ResponsePromise;
import com.honeytech.kafka_future_commons.exception.AsyncExecutionException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


/**
 * Service for saving request context and processing async response
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class RequestResponseService {

    /**
     * Response promises
     */
    @Getter
    private ConcurrentMap<UUID, ResponsePromise> responsePromises = new ConcurrentHashMap<>();


    /**
     * Method completes response promise with message
     * @param chainId Chain identifier
     * @param correlationId Correlation identifier
     * @param message Response message
     * @param headers Response headers
     */
    public void completeWithResponse(UUID chainId, UUID correlationId, Message message, Map<String, Object> headers) {
        log.debug("Process response for chain id {}. Received correlation id {}", chainId, correlationId);
        ResponsePromise responsePromise = getResponsePromise(chainId);
        log.debug("Found response promise for chain id {}", chainId);

        if (responsePromise == null) {
            log.warn("Promise for chain id {} not found", chainId);
            return;
        }
        if (LocalDateTime.now().isBefore(responsePromise.requestExpiringTime())) {
            Optional.ofNullable(responsePromise.responses().get(correlationId))
                    .orElseThrow(() -> {
                        log.warn("Unknown correlation id {}", correlationId);
                        return new AsyncExecutionException("Unknown correlation id " + correlationId);
                    }).complete(new AsyncResponse<>(message, headers));
            log.debug("Response added to response promise");
            return;
        }
        log.warn("Request timeout with chain id {}", chainId);

        if (responsePromise.timeoutCallback() != null && !responsePromise.result().isDone()) {
            responsePromise.timeoutCallback().run();
        }
        log.debug("Sending error message with chain id {}", chainId);
        responsePromise.result().completeExceptionally(new AsyncExecutionException("Request timeout"));

    }


    /**
     * Method removes promise
     * @param chainId Chain identifier
     * @return Response promise
     */
    public ResponsePromise removeResponsePromise(UUID chainId) {
        log.debug("Removing response promise with chain id {}", chainId);
        return responsePromises.remove(chainId);
    }


    /**
     * Scheduled method to remove expired promises
     */
    @Scheduled(initialDelayString = "${chain.threshold:60000}", fixedDelayString = "${chain.threshold:60000}")
    public void removeExpiredPromise() {
        log.debug("Checking request timeouts...");
        List<UUID> forRemove = new LinkedList<>();

        this.responsePromises.forEach((chainId, responsePromise) -> {
            if (responsePromise.requestExpiringTime().isBefore(LocalDateTime.now())) {
                forRemove.add(chainId);
            }
        });

        for (UUID chainId : forRemove) {
            ResponsePromise responsePromise = removeResponsePromise(chainId);
            if (responsePromise == null) {
                continue;
            }
            log.debug("Removing response promise with chain id {}", chainId);

            if (responsePromise.timeoutCallback() != null && !responsePromise.result().isDone()) {
                responsePromise.timeoutCallback().run();
                log.debug("Sending error message with chain id {}", chainId);
            }
        }
    }

    /**
     * Method returns promise
     * @param chainId Chain identifier
     * @return Response promise
     */
    public ResponsePromise getResponsePromise(UUID chainId) {
        return responsePromises.get(chainId);
    }
}
