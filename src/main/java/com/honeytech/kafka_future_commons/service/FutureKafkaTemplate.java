package com.honeytech.kafka_future_commons.service;

import com.honeytech.kafka_future_commons.exception.AsyncExecutionException;
import com.honeytech.kafka_future_commons.model.AsyncResponse;
import com.honeytech.kafka_future_commons.model.Headers;
import com.honeytech.kafka_future_commons.model.RequestParams;
import com.honeytech.kafka_future_commons.model.ResponsePromise;
import com.honeytech.kafka_future_commons.util.KafkaHeaderAccessor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static com.honeytech.kafka_future_commons.util.KafkaHelper.uuidToBytes;

/**
 * Class for chaining micro-service operations
 * @param <K> - kafka key type
 * @param <V> - kafka value type
 */
@RequiredArgsConstructor
@Slf4j
public class FutureKafkaTemplate<K, V> {

    private final RequestResponseService requestResponseService;

    @Getter
    private final KafkaTemplate<K, V> kafkaTemplate;


    @Value("${chain.threshold:60000}")
    private long THRESHOLD_MS;

    /**
     * Method sends messages to kafka
     * @param records messages
     * @param requestParams request parameters
     */
    public CompletableFuture<List<AsyncResponse>> send(List<ProducerRecord<K, V>> records, RequestParams requestParams) {
        return send(records, requestParams, THRESHOLD_MS, ChronoUnit.MILLIS, () -> {});
    }

    /**
     * Method sends message to kafka
     * @param record message
     * @param requestParams request parameters
     * @param timeout timeout
     * @param timeUnit time unit
     * @param timeoutCallback timeout callback
     */
    public CompletableFuture<List<AsyncResponse>> send(ProducerRecord<K, V> record, RequestParams requestParams, Long timeout, TemporalUnit timeUnit, Runnable timeoutCallback) {
        return send(Collections.singletonList(record), requestParams, timeout, timeUnit, timeoutCallback);
    }

    /**
     * Method sends message to kafka
     * @param record message
     * @param requestParams request parameters
     */
    public CompletableFuture<List<AsyncResponse>> send(ProducerRecord<K, V> record, RequestParams requestParams) {
        return send(Collections.singletonList(record), requestParams);
    }

    /**
     * Method sends mesage to kafka
     * @param records messages
     * @param requestParams request parameters
     * @param timeout timeout
     * @param timeUnit time unit
     * @param timeoutCallback timeout callback
     * @return promise of result
     */
    public CompletableFuture<List<AsyncResponse>> send(List<ProducerRecord<K, V>> records,
                                                       RequestParams requestParams,
                                                       Long timeout,
                                                       TemporalUnit timeUnit,
                                                       Runnable timeoutCallback) {

        if (CollectionUtils.isEmpty(records)) {
            throw new IllegalArgumentException("Empty records! Nothing to send");
        }
        UUID chainId = Optional.ofNullable(requestParams.chainId())
                .orElseThrow(() -> new IllegalArgumentException("Chain id must be set"));
        records.forEach(record -> {
            record.headers().add(Headers.CHAIN_ID.name(), uuidToBytes(chainId));
            Optional.ofNullable(KafkaHeaderAccessor.ofHeaders(record.headers()).messageId())
                    .orElseThrow(() -> new IllegalArgumentException(String.format("Empty %s header", Headers.MESSAGE_ID.name())));
        });
        final CompletableFuture<List<AsyncResponse>> result = initializePromiseResult(
                createResponsePromise(records, requestParams, timeout, timeUnit, timeoutCallback));

        boolean isSuccessfullySent = kafkaTemplate.executeInTransaction(template -> {
            records.forEach(template::send);
            return true;
        });
        if (isSuccessfullySent) {
            log.debug("All requests have been sent. Awaiting responses");
            return result;
        }

        this.requestResponseService.removeResponsePromise(chainId);
        log.error("Couldn't send requests to kafka");
        throw new AsyncExecutionException("Couldn't send requests to kafka");
    }

    /**
     * Method initializes response promise
     * @param records message records
     * @param requestParams request parameters
     * @param timeout timeout
     * @param timeUnit time unit
     * @param timeoutCallback timeout callback
     * @return response promise
     */
    private ResponsePromise createResponsePromise(List<ProducerRecord<K, V>> records,
                                                  RequestParams requestParams,
                                                  Long timeout,
                                                  TemporalUnit timeUnit,
                                                  Runnable timeoutCallback) {

        if (requestResponseService.getResponsePromises().get(requestParams.chainId()) != null) {
            throw new IllegalStateException(String.format("Response promise with chain id %s is already awaiting response!",
                    requestParams.chainId()));
        }
        ResponsePromise responsePromise = new ResponsePromise()
                .chainId(requestParams.chainId())
                .isRootRequest(requestParams.isRootRequest())
                .timeoutCallback(timeoutCallback)
                .requestExpiringTime(LocalDateTime.now().plus(timeout, timeUnit))
                .incomeHeaders(requestParams.rootHeaders().toMap())
                .incomeMessage(requestParams.incomeMessage());
        records.forEach(record -> {
            KafkaHeaderAccessor headerAccessor = KafkaHeaderAccessor.ofHeaders(record.headers());
            responsePromise.requestHeaders().put(headerAccessor.messageId(), headerAccessor);
            responsePromise.responses().put(headerAccessor.messageId(), new CompletableFuture<>());
        });

        this.requestResponseService.getResponsePromises().put(Optional.ofNullable(requestParams.chainId())
                        .orElseThrow(() -> new AsyncExecutionException("chain id is null")),
                responsePromise);
        log.debug("ResponsePromise with chain id {} is initialized", requestParams.chainId());
        return responsePromise;
    }

    /**
     * Method initializes promise result
     * @see CompletableFuture#allOf(CompletableFuture[])
     * @see CompletableFuture#join()
     * @param responsePromise response promise
     * @return common result promise
     */
    private CompletableFuture<List<AsyncResponse>> initializePromiseResult(ResponsePromise responsePromise) {
        List<CompletableFuture<AsyncResponse>> responsePromises = new ArrayList<>(responsePromise.responses().values());

        final CompletableFuture<List<AsyncResponse>> result = CompletableFuture
                .allOf(responsePromises.toArray(new CompletableFuture[0]))
                .thenApply(v -> responsePromises.stream().map(CompletableFuture::join).collect(Collectors.toList()));

        responsePromise.result(result);
        return result;
    }
}
