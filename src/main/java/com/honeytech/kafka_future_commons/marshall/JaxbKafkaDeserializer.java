package com.honeytech.kafka_future_commons.marshall;

import com.honeytech.kafka_future_commons.util.KafkaHeaderAccessor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SerializationException;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.function.Function;

/**
 * Kafka JAXB body deserializer
 */
@Slf4j
public class JaxbKafkaDeserializer<T> implements Deserializer<T> {

    public static final String JAXB_CONTENT_PATH = "jaxb.kafka.deserializer.config.context.path";

    public static final String JAXB_TYPE_MAPPER = "jaxb.kafka.deserializer.config.type.mapper";

    private String jaxbContentPath;

    private Function<Headers, Class<?>> typeMapper;

    /**
     * Configure this class.
     * @param configs configs in key/value pairs
     * @param isKey whether is for key or value
     */
    @Override
    @SuppressWarnings("unchecked")
    public void configure(Map<String, ?> configs, boolean isKey) {
        if (configs.containsKey(JAXB_CONTENT_PATH)) {
            jaxbContentPath = (String) configs.get(JAXB_CONTENT_PATH);
        }
        if (configs.containsKey(JAXB_TYPE_MAPPER)) {
            typeMapper = (Function<Headers, Class<?>>) configs.get(JAXB_TYPE_MAPPER);
        }
    }

    /**
     * Deserialize xml message to object;
     * Target class defined in header __TypeId__
     * @param topic topic associated with the data
     * @param headers message headers
     * @param data serialized bytes
     * @return deserialized type data
     */
    @SuppressWarnings("unchecked")
    public T deserialize(String topic, Headers headers, byte[] data) throws ClassNotFoundException, IOException {
        Class<?> targetClass = null;

        if (typeMapper != null) {
            targetClass = typeMapper.apply(headers);
        }
        if (targetClass == null) {
            KafkaHeaderAccessor headerAccessor = KafkaHeaderAccessor.ofHeaders(headers);
            if (headerAccessor.defaultClassIdFieldName() != null) {
                targetClass = Class.forName(headerAccessor.defaultClassIdFieldName());
            }
        }
        if (targetClass == null) {
            return deserialize(topic, data);
        }
        try (final ByteArrayInputStream inputStream = new ByteArrayInputStream(data)) {
            return (T) JAXB.unmarshal(inputStream, targetClass);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public T deserialize(String topic, byte[] data) {
        try {
            final JAXBContext jaxbContext = JAXBContext.newInstance(jaxbContentPath);
            final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            try (final ByteArrayInputStream inputStream = new ByteArrayInputStream(data)){
                return (T) unmarshaller.unmarshal(inputStream);
            }
        } catch (Exception exception) {
            throw new SerializationException(exception);
        }
    }

    @Override
    public void close() {}
}
