package com.honeytech.kafka_future_commons.marshall;

import com.honeytech.kafka_future_commons.exception.DeserializationErrorMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.ExtendedDeserializer;

import java.util.Arrays;
import java.util.Map;

/**
 * Kafka body deserializer with json support
 */
@Slf4j
public class KafkaDeserializer<T> extends KafkaSerializerBase implements ExtendedDeserializer<T> {

    private JsonKafkaDeserializer<T> jsonDeserializer = new JsonKafkaDeserializer<>();

    private JaxbKafkaDeserializer<T> jaxbDeserializer = new JaxbKafkaDeserializer<>();


    /**
     * Configure this class.
     * @param configs configs in key/value pairs
     * @param isKey whether is for key or value
     */
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        jsonDeserializer.configure(configs, isKey);
        jaxbDeserializer.configure(configs, isKey);
    }

    /**
     * Deserialize a record value from a byte array into a value or object.
     * @param topic topic associated with the data
     * @param headers headers associated with the record; may be empty.
     * @param data serialized bytes; may be null; implementations are recommended to handle null by returning a value or null rather than throwing an exception.
     * @return deserialized typed data; may be null
     */
    @SuppressWarnings("unchecked")
    @Override
    public T deserialize(String topic, Headers headers, byte[] data) {
        try {
            if (isXmlType(headers)) {
                return jaxbDeserializer.deserialize(topic, headers, data);
            } else {
                return jsonDeserializer.deserialize(topic, headers, data);
            }
        } catch (final Exception exception) {
            log.error("Error on process message [" + Arrays.toString(data) + "] from topic [" + topic + "]", exception);
            return (T) new DeserializationErrorMessage(new String(data), exception);
        }
    }

    /**
     * Deserialize a record value from a byte array into a value or object.
     * @param topic topic associated with the data
     * @param data serialized bytes; may be null; implementations are recommended to handle null by returning a value or null rather than throwing an exception.
     * @return deserialized typed data; may be null
     */
    @Override
    public T deserialize(String topic, byte[] data) {
        return null;
    }

    @Override
    public void close() {}
}
