package com.honeytech.kafka_future_commons.marshall;


import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.ExtendedSerializer;

import java.util.Map;

/**
 * Kafka body serializer with json support
 */
public class KafkaSerializer<T> extends KafkaSerializerBase implements ExtendedSerializer<T> {

    private JsonKafkaSerializer<T> jsonSerializer = new JsonKafkaSerializer<>();

    private JaxbKafkaSerializer<T> jaxbSerializer = new JaxbKafkaSerializer<>();


    /**
     * Configure this class.
     * @param configs configs in key/value pairs
     * @param isKey whether is for key or value
     */
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        jsonSerializer.configure(configs, isKey);
        jaxbSerializer.configure(configs, isKey);
    }


    /**
     * Convert {@code data} into a byte array.
     *
     * @param topic topic associated with data
     * @param data typed data
     * @return serialized bytes
     */
    @Override
    public byte[] serialize(String topic, Headers headers, T data) {
        return isXmlType(headers)
                ? jaxbSerializer.serialize(topic, data)
                : jsonSerializer.serialize(topic, headers, data);
    }

    /**
     * Convert {@code data} into a byte array.
     *
     * @param topic topic associated with data
     * @param data typed data
     * @return serialized bytes
     */
    @Override
    public byte[] serialize(String topic, T data) {
        return null;
    }

    /**
     * Close this serializer.
     *
     * This method must be idempotent as it may be called multiple times.
     */
    @Override
    public void close() {}
}
