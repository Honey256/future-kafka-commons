package com.honeytech.kafka_future_commons.marshall;

import org.apache.commons.lang3.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.util.Map;

/**
 * Kafka JAXB body serializer
 */
public class JaxbKafkaSerializer<T> implements Serializer<T> {

    /**
     * Configure this class.
     * @param configs configs in key/value pairs
     * @param isKey whether is for key or value
     */
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {}

    /**
     * Convert {@code data} into a byte array.
     *
     * @param topic topic associated with data
     * @param data typed data
     * @return serialized bytes
     */
    @Override
    public byte[] serialize(String topic, T data) {
        try {
            final JAXBContext jaxbContext = JAXBContext.newInstance(data.getClass());
            final Marshaller marshaller = jaxbContext.createMarshaller();

            try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()){
                marshaller.marshal(data, outputStream);
                return outputStream.toByteArray();
            }
        } catch (final Exception exception) {
            throw new SerializationException(exception);
        }
    }

    /**
     * Close this serializer.
     *
     * This method must be idempotent as it may be called multiple times.
     */
    @Override
    public void close() {}
}
