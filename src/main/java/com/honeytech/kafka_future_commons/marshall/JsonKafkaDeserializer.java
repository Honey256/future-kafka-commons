package com.honeytech.kafka_future_commons.marshall;

import com.fasterxml.jackson.databind.JavaType;
import com.honeytech.kafka_future_commons.exception.AsyncExecutionException;
import org.apache.commons.lang3.SerializationException;
import org.apache.kafka.common.header.Headers;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.util.Assert;

import org.springframework.validation.Errors;

import javax.validation.ConstraintViolation;
import javax.validation.ElementKind;
import javax.validation.Path;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Kafka json body deserializer
 */
public class JsonKafkaDeserializer<T> extends JsonDeserializer<T> {

    private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    /**
     * Deserialize a record value from a byte array into a value or object.
     * @param topic topic associated with the data
     * @param headers headers associated with the record; may be empty.
     * @param data serialized bytes; may be null; implementations are recommended to handle null by returning a value or null rather than throwing an exception.
     * @return deserialized typed data; may be null
     */
    @Override
    public T deserialize(final String topic, final Headers headers, final byte[] data) {
        if (data == null) {
            return null;
        }
        try {
            JavaType javaType = this.typeMapper.toJavaType(headers);
            T deserializedMessage;
            if (javaType == null) {
                Assert.state(this.targetType != null, "No type information in headers and no default type provided");
                deserializedMessage = deserialize(topic, data);
            } else {
                deserializedMessage = this.objectMapper.readerFor(javaType).readValue(data);
            }
            validateObject(deserializedMessage);
            return deserializedMessage;

        } catch (final Exception exception) {
            throw new SerializationException(exception);
        }
    }

    /**
     * Method validates object on constraints
     */
    private void validateObject(final T obj) {
        final Set<ConstraintViolation<T>> violations = validator.validate(obj);

        if (!violations.isEmpty()) {
            throw new AsyncExecutionException(violations.stream()
                            .map(constraintViolation -> String.format(
                                    "Field: %s; Violation info: %s",
                                    determineField(constraintViolation),
                                    constraintViolation.toString()))
                            .collect(Collectors.joining("|")));
        }
    }

    /**
     * Determine a field for the given constraint violation.
     * The default implementation returns stringified property path
     *
     * @param violation the current JSR-303 ConstraintViolation
     * @return the Spring reported field (for use with {@link Errors})
     */
    private String determineField(ConstraintViolation<T> violation) {
        StringBuilder stringBuilder = new StringBuilder();
        boolean first = true;
        for (Path.Node node : violation.getPropertyPath()) {
            if (node.isInIterable()) {
                stringBuilder.append('[');
                Object index = node.getIndex();
                if (index == null) {
                    index = node.getKey();
                }
                if (index != null) {
                    stringBuilder.append(index);
                }
                stringBuilder.append(']');
            }
            String name = node.getName();
            if (name != null && node.getKind() == ElementKind.PROPERTY && !name.startsWith("<")) {
                if (!first) {
                    stringBuilder.append('.');
                }
                first = false;
                stringBuilder.append(name);
            }
        }
        return stringBuilder.toString();
    }
}
