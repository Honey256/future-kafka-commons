package com.honeytech.kafka_future_commons.marshall;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.honeytech.kafka_future_commons.config.SpringContextHolder;
import org.springframework.kafka.support.serializer.JsonSerializer;

/**
 * Kafka json body serializer
 */
public class JsonKafkaSerializer<T> extends JsonSerializer<T> {

    public JsonKafkaSerializer() {
        super((ObjectMapper) SpringContextHolder.getAppContext().getBean("kafkaObjectMapper"));
    }
}
