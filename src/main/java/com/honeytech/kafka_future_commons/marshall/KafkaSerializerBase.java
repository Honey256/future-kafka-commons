package com.honeytech.kafka_future_commons.marshall;

import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.Headers;

import java.util.Arrays;
import java.util.Objects;

import static com.honeytech.kafka_future_commons.model.Headers.CONTENT_TYPE;

/**
 * Base kafka serializer
 */
public class KafkaSerializerBase {

    /**
     * Method returns header as string
     */
    protected String headerAsString(final Headers headers, final String name) {
        return Arrays.stream(headers.toArray())
                .filter(Objects::nonNull)
                .filter(header -> Objects.equals(header.key(), name))
                .findAny()
                .map(Header::value)
                .map(String::new)
                .orElse(null);
    }

    /**
     * Method checks if message type is XML
     * @param headers message headers
     * @return xml sign
     */
    protected boolean isXmlType(final Headers headers) {
        return "application/xml".equalsIgnoreCase(headerAsString(headers, CONTENT_TYPE.name()));
    }
}
