package com.honeytech.kafka_future_commons.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation needed to process future result correctly
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AsyncRequest {

    /**
     * @return is auto acknowledgement needed
     */
    boolean autoAck() default false;
}
