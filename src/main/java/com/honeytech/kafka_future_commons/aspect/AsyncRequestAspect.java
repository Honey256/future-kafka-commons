package com.honeytech.kafka_future_commons.aspect;

import com.honeytech.kafka_future_commons.config.DefaultKafkaConfig;
import com.honeytech.kafka_future_commons.exception.ChainErrorException;
import com.honeytech.kafka_future_commons.exception.KafkaErrorHandler;
import com.honeytech.kafka_future_commons.model.Message;
import com.honeytech.kafka_future_commons.service.KafkaClient;
import com.honeytech.kafka_future_commons.util.KafkaHeaderAccessor;
import com.honeytech.kafka_future_commons.util.KafkaHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * Aspect processes response from kafka handler
 */
@Aspect
@Component
@RequiredArgsConstructor
@Order(Ordered.LOWEST_PRECEDENCE)
@Slf4j
public class AsyncRequestAspect {

    private final KafkaHelper kafkaHelper;

    private final DefaultKafkaConfig kafkaConfig;

    private final KafkaClient kafkaClient;

    private final KafkaErrorHandler kafkaErrorHandler;

    /**
     * Method handles kafka response
     * @param joinPoint join point
     * @param asyncRequest annotation
     * @return handler response
     */
    @Around("@annotation(AsyncRequest)")
    public Object processAsyncRequest(ProceedingJoinPoint joinPoint, AsyncRequest asyncRequest) throws Throwable {
        final boolean autoAck = asyncRequest.autoAck();
        final List<Acknowledgment> acknowledgments = Collections.singletonList(getAcknowledgement(joinPoint));
        final Map<String, Object> headers = getHeaders(joinPoint);
        final Message requestMessage = getMessage(joinPoint);
        Object result = joinPoint.proceed();

        if (result instanceof CompletableFuture) {
            processFutureResult((CompletableFuture) result, headers, requestMessage, acknowledgments, autoAck);
            return null;
        }
        return result;
    }

    /**
     * Method processes future result. If exception - process error; Else - reply on request
     * @param future response promise
     * @param headers headers
     * @param requestMessage request message
     * @param acknowledgments acknowledgement
     * @param autoAck is auto acknowledgement needed
     */
    @SuppressWarnings("unchecked")
    private void processFutureResult(CompletableFuture future,
                                     Map<String, Object> headers,
                                     Message requestMessage,
                                     List<Acknowledgment> acknowledgments,
                                     boolean autoAck) {
        future.whenComplete((response, exception) -> {
            if (exception instanceof Throwable) {
                processRequestError(requestMessage, (Throwable) exception, headers);
            } else if (response != null) {
                replyOnRequest(response, headers, requestMessage);
            }
            if (autoAck) {
                acknowledgments.get(0).acknowledge();
            }
        });
    }

    /**
     * Method replies with response
     * @param response response
     * @param headers headers
     */
    private void replyOnRequest(Object response, Map<String, Object> headers, Message request) {
        if (response == null) {
            return;
        }
        if (response instanceof Message) {
            kafkaClient.send(
                    (Message) response,
                    kafkaHelper.reverseHeaders(headers, kafkaConfig.getSpecificConsumer().getGroupId()));
        }
        if (response instanceof GenericMessage && Message.class.equals(((GenericMessage) response).getPayload().getClass())) {
            kafkaClient.send(
                    (Message) ((GenericMessage) response).getPayload(),
                    kafkaHelper.reverseHeaders(headers, kafkaConfig.getSpecificConsumer().getGroupId())
            );
        }
        processRequestError(request, new IllegalArgumentException("Unknown response type"), headers);
    }

    /**
     * Method processes request errors
     * @param exception exception
     * @param headers headers
     */
    private <U> void processRequestError(U message, Throwable exception, Map<String, Object> headers) {
        final KafkaHeaderAccessor headerAccessor = KafkaHeaderAccessor.ofMap(headers);
        if (!(exception instanceof ChainErrorException)) {
            kafkaErrorHandler.sendToErrorTopic(message, headerAccessor, (Exception) exception);
        }
        kafkaErrorHandler.sendToUpstreamService(headerAccessor, (Exception) exception);
    }

    /**
     * Method extracts acknowledgements from handler args
     * @param joinPoint join point
     * @return acknowledgement
     */
    private Acknowledgment getAcknowledgement(ProceedingJoinPoint joinPoint) {
        Acknowledgment acknowledgment = null;
        for (Object object : joinPoint.getArgs()) {
            if (object instanceof Acknowledgment) {
                acknowledgment = (Acknowledgment) object;
            }
        }
        if (acknowledgment == null) {
            throw new IllegalArgumentException("Acknowledgement is null");
        }
        return acknowledgment;
    }

    /**
     * Method extracts headers from handler args
     * @param joinPoint join point
     * @return headers
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> getHeaders(ProceedingJoinPoint joinPoint) {
        Map<String, Object> headers = new HashMap<>();
        for (Object object : joinPoint.getArgs()) {
            if (object.getClass().isAnnotationPresent(Headers.class)) {
                headers.putAll((Map<String, Object>) object);
            }
        }
        if (MapUtils.isEmpty(headers)) {
            throw new IllegalArgumentException("Headers is empty");
        }
        return headers;
    }


    /**
     * Method extracts request message from handler args
     * @param joinPoint join point
     * @return request message
     */
    @SuppressWarnings("unchecked")
    private <U extends Message> U getMessage(ProceedingJoinPoint joinPoint) {
        U message = null;
        for (Object object : joinPoint.getArgs()) {
            if (object instanceof Message) {
                message = (U) object;
            }
        }
        if (message == null) {
            throw new IllegalArgumentException("Message is empty");
        }
        return message;
    }
}
