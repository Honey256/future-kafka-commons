package com.honeytech.kafka_future_commons.util;

import com.honeytech.kafka_future_commons.config.DefaultKafkaConfig;
import com.honeytech.kafka_future_commons.exception.AsyncExecutionException;
import com.honeytech.kafka_future_commons.model.AsyncResponse;
import com.honeytech.kafka_future_commons.model.Headers;
import com.honeytech.kafka_future_commons.model.Message;
import com.honeytech.kafka_future_commons.model.RequestParams;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class KafkaHelper {

    private final DefaultKafkaConfig kafkaConfig;

    public static UUID bytesToUUID(byte[] bytes) {
        if (bytes == null || bytes.length != 16) {
            throw new IllegalArgumentException("Invalid bytes");
        }
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        long high = byteBuffer.getLong();
        long low = byteBuffer.getLong();
        return new UUID(high, low);
    }

    public static byte[] uuidToBytes(UUID uuid) {
        if (uuid == null) {
            throw new IllegalArgumentException("uuid is null");
        }

        ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[16]);
        byteBuffer.putLong(uuid.getMostSignificantBits());
        byteBuffer.putLong((uuid.getLeastSignificantBits()));
        return byteBuffer.array();
    }

    public static byte[] intToBytes(Integer integer) {
        if (integer == null) {
            throw new IllegalArgumentException("Integer is null");
        }
        ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[4]);

        byteBuffer.putInt(integer);
        return byteBuffer.array();
    }

    public static Integer bytesToInt(byte[] bytes) {
        if (bytes == null || bytes.length > 4) {
            throw new IllegalArgumentException("Invalid bytes");
        }

        return ByteBuffer.wrap(bytes).getInt();
    }

    public Map<String, Object> reverseHeaders(Map<String, Object> incomeHeaders, String sourceInstanceId) {
        if (StringUtils.isBlank(sourceInstanceId)) {
            throw new IllegalArgumentException("Empty source instance identifier");
        }
        KafkaHeaderAccessor headers = KafkaHeaderAccessor.ofMap(incomeHeaders);
        if (StringUtils.isBlank(headers.replyTopic())) {
            throw new AsyncExecutionException("Reply topic is empty");
        }
        KafkaHeaderAccessor reversedHeaders = KafkaHeaderAccessor.createIdentifiedWithSuccess()
                .sourceInstanceId(sourceInstanceId)
                .correlationId(headers.messageId())
                .chainId(headers.chainId())
                .topic(headers.replyTopic())
                .guiSessionId(headers.guiSessionId())
                .traceId(headers.traceId());

        if (headers.replyTopic().contains("specific")) {
            reversedHeaders.destinationInstanceId(headers.sourceInstanceId());
        }
        return reversedHeaders.toMap();
    }

    public void printHeaders(Map<String, Object> headers) {
        log.debug("Headers: {}", KafkaHeaderAccessor.ofMap(headers).toString());
    }

    public ProducerRecord<String, Message> prepareProducerRecord(Message message, String topic) {
        ProducerRecord<String, Message> producerRecord = new ProducerRecord<>(topic, UUID.randomUUID().toString(), message);
        producerRecord.headers().add(Headers.SOURCE_INSTANCE_ID.name(), kafkaConfig.getSpecificConsumer().getGroupId().getBytes());
        producerRecord.headers().add(KafkaHeaders.REPLY_TOPIC, kafkaConfig.getSpecificConsumer().getTopic().getBytes());
        producerRecord.headers().add(Headers.MESSAGE_ID.name(), KafkaHelper.uuidToBytes(UUID.randomUUID()));
        return producerRecord;
    }

    public List<ProducerRecord<String, Message>> prepareProducerRecords(List<? extends Message> messages, String topic, RequestParams requestParams) {
        return messages.stream()
                .map(message -> prepareProducerRecord(message, topic))
                .collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    public static <U extends Message> U getSingleResponseFromList(@NotNull List<AsyncResponse> messages) {
        return (U) messages.stream()
                .findFirst()
                .map(AsyncResponse::message)
                .orElseThrow(() -> new IllegalArgumentException("Response message is null"));
    }

    @SuppressWarnings("unchecked")
    public static <U extends Message> List<U> getAllResponsesFromList(@NotNull List<AsyncResponse> messages) {
        return messages.stream()
                .map(AsyncResponse::message)
                .map(message -> (U) message)
                .collect(Collectors.toList());
    }
}
