package com.honeytech.kafka_future_commons.util;


import com.honeytech.kafka_future_commons.model.Headers;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.converter.AbstractJavaTypeMapper;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.honeytech.kafka_future_commons.util.KafkaHelper.bytesToInt;
import static com.honeytech.kafka_future_commons.util.KafkaHelper.bytesToUUID;
import static com.honeytech.kafka_future_commons.util.KafkaHelper.intToBytes;
import static com.honeytech.kafka_future_commons.util.KafkaHelper.uuidToBytes;

/**
 * Accessor for kafka headers
 */
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Accessors(chain = true, fluent = true)
public class KafkaHeaderAccessor {

    private KafkaHelper kafkaHelper;

    /**
     * Instance of source service
     */
    private String sourceInstanceId;

    /**
     * Instance of destination service
     */
    private String destinationInstanceId;

    /**
     * Chain identifier
     */
    private UUID chainId;

    /**
     * Message identifier
     */
    private UUID messageId;

    /**
     * Message identifier from income request
     */
    private UUID correlationId;

    /**
     * 0 - success,= or else - error
     */
    private Integer processingResult;

    /**
     * Description of error
     */
    private String errorDescription;

    /**
     * Error service
     */
    private String errorServiceOccurred;

    /**
     * Reply topic
     */
    private String replyTopic;

    /**
     * Kafka topic
     */
    private String topic;

    /**
     * Absolute class name for deserialization purpose
     */
    private String defaultClassIdFieldName;

    /**
     * Content type of message body. Equals 'application/xml' for xml, empty for json
     */
    private String contentType;

    /**
     * Service method to me executed, if service provide more action
     */
    private String methodName;

    /**
     * GUI session identifier. Must be not empty if initial action originated in UI
     */
    private String guiSessionId;

    /**
     * Identifier for tracing messages between microservices
     */
    private String traceId;

    public Map<String, Object> toMap() {
        Map<String, Object> headers = new HashMap<>();

        if(sourceInstanceId != null) {
            headers.put(Headers.SOURCE_INSTANCE_ID.name(), sourceInstanceId.getBytes());
        }
        if(destinationInstanceId != null) {
            headers.put(Headers.DESTINATION_INSTANCE_ID.name(), destinationInstanceId.getBytes());
        }
        if(chainId != null) {
            headers.put(Headers.CHAIN_ID.name(), uuidToBytes(chainId));
        }
        if(messageId != null) {
            headers.put(Headers.MESSAGE_ID.name(), uuidToBytes(messageId));
        }
        if(correlationId != null) {
            headers.put(Headers.CORRELATION_ID.name(), uuidToBytes(correlationId));
        }
        if(processingResult != null) {
            headers.put(Headers.PROCESSING_RESULT.name(), intToBytes(processingResult));
        }
        if(errorDescription != null) {
            headers.put(Headers.ERROR_DESCRIPTION.name(), errorDescription.getBytes(StandardCharsets.UTF_8));
        }
        if(errorServiceOccurred != null) {
            headers.put(Headers.ERROR_SERVICE_OCCURRED.name(), errorServiceOccurred.getBytes());
        }
        if(replyTopic != null) {
            headers.put(KafkaHeaders.REPLY_TOPIC, replyTopic.getBytes());
        }
        if(topic != null) {
            headers.put(KafkaHeaders.TOPIC, topic.getBytes());
        }
        if(defaultClassIdFieldName != null) {
            headers.put(AbstractJavaTypeMapper.DEFAULT_CLASSID_FIELD_NAME, defaultClassIdFieldName.getBytes());
        }
        if (contentType != null) {
            headers.put(Headers.CONTENT_TYPE.name(), contentType.getBytes());
        }
        if(methodName != null) {
            headers.put(Headers.METHOD_NAME.name(), methodName.getBytes());
        }
        if(guiSessionId != null) {
            headers.put(Headers.GUI_SESSION_ID.name(), guiSessionId.getBytes());
        }
        if(traceId != null) {
            headers.put(Headers.TRACE_ID.name(), traceId.getBytes());
        }
        return headers;
    }

    public org.apache.kafka.common.header.Headers toKafkaHeaders() {
        RecordHeaders recordHeaders = new RecordHeaders();
        toMap().forEach((key, value) -> recordHeaders.add(key, (byte[]) value));
        return recordHeaders;
    }

    public static KafkaHeaderAccessor ofMap(Map<String, Object> headers) {
        if (MapUtils.isEmpty(headers)) {
            throw new IllegalArgumentException("No headers found");
        }

        Object sourceInstanceId = headers.get(Headers.SOURCE_INSTANCE_ID.name());
        Object destinationInstanceId = headers.get(Headers.DESTINATION_INSTANCE_ID.name());
        Object chainId = headers.get(Headers.CHAIN_ID.name());
        Object messageId = headers.get(Headers.MESSAGE_ID.name());
        Object correlationId = headers.get(Headers.CORRELATION_ID.name());
        Object processingResult = headers.get(Headers.PROCESSING_RESULT.name());
        Object errorDescription = headers.get(Headers.ERROR_DESCRIPTION.name());
        Object errorServiceOccurred = headers.get(Headers.ERROR_SERVICE_OCCURRED.name());
        Object replyTopic = headers.get(KafkaHeaders.REPLY_TOPIC);
        Object topic = headers.get(KafkaHeaders.TOPIC);
        Object classId = headers.get(AbstractJavaTypeMapper.DEFAULT_CLASSID_FIELD_NAME);
        Object contentType = headers.get(Headers.CONTENT_TYPE.name());
        Object methodName = headers.get(Headers.METHOD_NAME.name());
        Object guiSessionId = headers.get(Headers.GUI_SESSION_ID.name());
        Object traceId = headers.get(Headers.TRACE_ID.name());

        return new KafkaHeaderAccessor()
                .sourceInstanceId(sourceInstanceId != null ? new String((byte[]) sourceInstanceId) : null)
                .destinationInstanceId(destinationInstanceId != null ? new String((byte[]) destinationInstanceId) : null)
                .chainId(chainId != null ? bytesToUUID((byte[]) chainId) : null)
                .messageId(messageId != null ? bytesToUUID((byte[]) messageId) : null)
                .correlationId(correlationId != null ? bytesToUUID((byte[]) chainId) : null)
                .processingResult(processingResult != null ? bytesToInt((byte[]) chainId) : null)
                .errorDescription(errorDescription != null ? new String((byte[]) errorDescription, StandardCharsets.UTF_8) : null)
                .errorServiceOccurred(errorServiceOccurred != null ? new String((byte[]) errorServiceOccurred) : null)
                .replyTopic(replyTopic != null ? new String((byte[]) replyTopic) : null)
                .topic(topic != null ? new String((byte[]) topic) : null)
                .defaultClassIdFieldName(classId != null ? new String((byte[]) classId) : null)
                .contentType(contentType != null ? new String((byte[]) contentType) : null)
                .methodName(methodName != null ? new String((byte[]) methodName) : null)
                .guiSessionId(guiSessionId != null ? new String((byte[]) guiSessionId) : null)
                .traceId(traceId != null ? new String((byte[]) traceId) : null);
    }

    public static KafkaHeaderAccessor ofHeaders(org.apache.kafka.common.header.Headers headers) {
        Map<String, Object> headerMap = new HashMap<>();
        headers.forEach(header -> headerMap.put(header.key(), header.value()));
        return ofMap(headerMap);
    }

    public static KafkaHeaderAccessor create() {
        return new KafkaHeaderAccessor();
    }

    public static KafkaHeaderAccessor createIdentified() {
        return new KafkaHeaderAccessor().messageId(UUID.randomUUID());
    }

    public static KafkaHeaderAccessor createIdentifiedWithSuccess() {
        return new KafkaHeaderAccessor()
                .messageId(UUID.randomUUID())
                .processingResult(0)
                .errorDescription("SUCCESS");
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
