package com.honeytech.kafka_future_commons.config;


import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Kafka properties for config file
 */
@Data
public class DefaultKafkaConfig {

    private Map<String, Object> properties;

    private String bootstrapServers;

    private String errorTopic;

    private Consumer groupConsumer;

    private Consumer specificConsumer;

    public DefaultKafkaConfig() {
        this.properties = new HashMap<>();
    }

    @Data
    public static class Consumer {

        private String groupId;

        private Long pollTimeout;

        private Integer concurrency;

        private String topic;

        private Map<String, Object> properties = new HashMap<>();
    }
}
