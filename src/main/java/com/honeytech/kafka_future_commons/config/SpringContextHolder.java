package com.honeytech.kafka_future_commons.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Component to hold spring context
 */
@Component
public class SpringContextHolder implements ApplicationContextAware {

    /**
     * Spring application context
     */
    public static ApplicationContext APPLICATION_CONTEXT;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        APPLICATION_CONTEXT = applicationContext;
    }

    public static ApplicationContext getAppContext() {
        return APPLICATION_CONTEXT;
    }
}
