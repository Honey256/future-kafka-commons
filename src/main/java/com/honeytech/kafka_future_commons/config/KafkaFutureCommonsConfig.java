package com.honeytech.kafka_future_commons.config;

import com.honeytech.kafka_future_commons.marshall.KafkaSerializer;
import com.honeytech.kafka_future_commons.model.Message;
import com.honeytech.kafka_future_commons.service.FutureKafkaTemplate;
import com.honeytech.kafka_future_commons.service.RequestResponseService;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Kafka producers config
 */
@Configuration
public class KafkaFutureCommonsConfig implements InitializingBean {

    @Value("instanceId")
    public String serviceInstanceId;

    @Autowired
    private DefaultKafkaConfig kafkaConfig;

    @Bean
    public KafkaTemplate<String, Message> messageKafkaTemplate() {
        return new KafkaTemplate<>(messageProducerFactory());
    }

    @Bean
    public ProducerFactory<String, Message> messageProducerFactory() {
        DefaultKafkaProducerFactory<String, Message> defaultKafkaProducerFactory = new DefaultKafkaProducerFactory<>(messageProducerConfigs());
        defaultKafkaProducerFactory.setTransactionIdPrefix(this.serviceInstanceId + "_tx");
        return defaultKafkaProducerFactory;
    }

    @Bean
    public Map<String, Object> messageProducerConfigs() {
        Map<String, Object> properties = new HashMap<>();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfig.getBootstrapServers());
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaSerializer.class);

        if (!kafkaConfig.getProperties().isEmpty()) {
            properties.putAll(kafkaConfig.getProperties());
        }
        return properties;
    }

    @Override
    public void afterPropertiesSet() {
        if (this.serviceInstanceId == null || this.kafkaConfig == null) {
            throw new IllegalStateException("KafkaBootstrapServer and serviceInstanceId and kafkaConfig MUST be set");
        }
    }
}
