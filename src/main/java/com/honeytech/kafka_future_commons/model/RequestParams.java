package com.honeytech.kafka_future_commons.model;

import com.honeytech.kafka_future_commons.util.KafkaHeaderAccessor;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;
import java.util.UUID;

/**
 * Parameters of request
 */
@Data
@Accessors(chain = true, fluent = true)
public class RequestParams {

    /**
     * Income request message
     */
    private final Message incomeMessage;

    /**
     * Root headers
     */
    private final KafkaHeaderAccessor rootHeaders;

    /**
     * Requests chain identifier
     */
    private final UUID chainId;

    /**
     * Sign of root chain
     */
    private final boolean isRootRequest;

    public RequestParams(Map<String, Object> rootHeaders) {
        KafkaHeaderAccessor accessor = KafkaHeaderAccessor.ofMap(rootHeaders);
        this.rootHeaders = accessor;
        this.chainId = accessor.chainId() == null ? UUID.randomUUID() : accessor.chainId();
        this.isRootRequest = accessor.chainId() == null;
        this.incomeMessage = null;
    }

    public RequestParams(Map<String, Object> rootHeaders, Message message) {
        KafkaHeaderAccessor accessor = KafkaHeaderAccessor.ofMap(rootHeaders);
        this.rootHeaders = accessor;
        this.chainId = accessor.chainId() == null ? UUID.randomUUID() : accessor.chainId();
        this.isRootRequest = accessor.chainId() == null;
        this.incomeMessage = message;
    }

    public RequestParams() {
        this.rootHeaders = KafkaHeaderAccessor.createIdentified();
        this.chainId = UUID.randomUUID();
        this.isRootRequest = true;
        this.incomeMessage = null;
    }
}
