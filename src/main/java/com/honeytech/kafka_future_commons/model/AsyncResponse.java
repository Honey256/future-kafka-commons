package com.honeytech.kafka_future_commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Map;


/**
 * Message with response and headers
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true, fluent = true)
public class AsyncResponse<T extends Message> {

    /**
     * Response message
     */
    private T message;

    /**
     * Headers
     */
    private Map<String, Object> headers;
}
