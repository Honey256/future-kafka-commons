package com.honeytech.kafka_future_commons.model;

/**
 * Kafka base DTO
 * All root DTO classes must implement this interface
 */
public interface Message {}
