package com.honeytech.kafka_future_commons.model;

/**
 * Kafka message headers
 */
public enum Headers {
    /**
     * Instance of source service
     */
    SOURCE_INSTANCE_ID,

    /**
     * Instance of destination service
     */
    DESTINATION_INSTANCE_ID,

    /**
     * Chain identifier
     */
    CHAIN_ID,

    /**
     * Message identifier
     */
    MESSAGE_ID,

    /**
     * Message identifier from income request
     */
    CORRELATION_ID,

    /**
     * 0 - success,= or else - error
     */
    PROCESSING_RESULT,

    /**
     * Description of error
     */
    ERROR_DESCRIPTION,

    /**
     * Error service
     */
    ERROR_SERVICE_OCCURRED,

    /**
     * Content type of message body. If empty - json
     */
    CONTENT_TYPE,

    /**
     * Service method to me executed, if service provide more action
     */
    METHOD_NAME,

    /**
     * GUI session identifier. Must be not empty if initial action originated in UI
     */
    GUI_SESSION_ID,

    /**
     * Identifier for tracing messages between microservices
     */
    TRACE_ID
}
