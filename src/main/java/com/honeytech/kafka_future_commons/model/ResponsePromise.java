package com.honeytech.kafka_future_commons.model;

import com.honeytech.kafka_future_commons.util.KafkaHeaderAccessor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Promise of response
 * Creates on sending request
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true, fluent = true)
public class ResponsePromise {

    /**
     * Chain id
     */
    private UUID chainId;

    /**
     * Response promises
     */
    private ConcurrentMap<UUID, CompletableFuture<AsyncResponse>> responses = new ConcurrentHashMap<>();

    /**
     * Responses promise
     */
    private CompletableFuture<List<AsyncResponse>> result;

    /**
     * Request headers
     */
    private ConcurrentMap<UUID, KafkaHeaderAccessor> requestHeaders = new ConcurrentHashMap<>();

    /**
     * Income headers
     */
    private Map<String, Object> incomeHeaders;

    /**
     * Income message
     */
    private Message incomeMessage;

    /**
     * Request timeout
     */
    private LocalDateTime requestExpiringTime;

    /**
     * Timeout callback
     */
    private Runnable timeoutCallback;

    /**
     * Sign of root request
     */
    private boolean isRootRequest;
}
